#!/usr/bin/env bash

APP_NAME=$1
CONFIG_ROOT='/var/deployment-env'

if [[ "$APP_NAME" == '' ]]; then
    echo 'usage: restart.sh <container>'
    exit 1
fi

mkdir -p $HOME/config/$APP_NAME
if [ -f "$CONFIG_ROOT"/sync-config.sh ]; then
    "$CONFIG_ROOT"/sync-config.sh $APP_NAME $HOME/config/$APP_NAME
fi

docker restart "$APP_NAME"
