#!/usr/bin/env bash

CONTAINER_OPTS=''
CONTAINER_ENV_OPTS=''
CONFIG_ROOT='/var/deployment-env'
REGISTRY_PREFIX=$(cat $CONFIG_ROOT/registry_prefix)

while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        -n|--name)
        APP_NAME="$2"
        shift
        shift
        ;;
        -i|--image)
        IMAGE_NAME="$2"
        shift
        shift
        ;;
        -v|--version)
        IMAGE_VERSION="$2"
        shift
        shift
        ;;
        -p|--port)
        CONTAINER_OPTS="$CONTAINER_OPTS -p $2:$2"
        shift
        shift
        ;;
        -e|--env)
        CONTAINER_OPTS="$CONTAINER_OPTS -e $2"
        shift
        shift
        ;;
        *)
        shift
        ;;
    esac
done

if [[ "$APP_NAME" == '' || "$IMAGE_NAME" == '' || "$IMAGE_VERSION" == '' ]]; then
    echo "usage: deploy.sh -n <application-name> -i <image-name> -v <image-version> [...]"
    exit 1
fi

function synchronize_config() {
    mkdir -p $HOME/config/$APP_NAME
    if [ -f "$CONFIG_ROOT"/sync-config.sh ]; then
        "$CONFIG_ROOT"/sync-config.sh $APP_NAME $HOME/config/$APP_NAME
    fi
}

function setup_environment_options() {
    if [ -f "$CONFIG_ROOT"/docker-opts ]; then
        opts="$(cat $CONFIG_ROOT/docker-opts)"
        opts="${opts//\{app_name\}/$APP_NAME}"

        CONTAINER_ENV_OPTS="$CONTAINER_ENV_OPTS $opts"
    fi
}

function ensure_previous_container_is_stopped() {
    docker stop $APP_NAME || true
    docker rm $APP_NAME || true
}

function start_container() {
    IMAGE="$REGISTRY_PREFIX/$IMAGE_NAME:$IMAGE_VERSION"
    docker pull $IMAGE
    docker run -d --restart=always --name $APP_NAME --user $UID \
        -v $HOME/config/$APP_NAME:/config:ro \
        -v /etc/hosts:/etc/hosts:ro \
        $CONTAINER_ENV_OPTS \
        $CONTAINER_OPTS \
        $IMAGE
}

synchronize_config
setup_environment_options
ensure_previous_container_is_stopped
start_container
